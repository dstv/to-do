package jzel.theo.co.za.todo.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;


/**
 * Created by jzeltheophiluis on 2018/02/02.
 */

public class CustomEditText extends AppCompatEditText {

    private Context context;
    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init(attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    public CustomEditText(Context context) {
        super(context);
        this.context = context;
        init(null);
    }

    public void init(AttributeSet attributeSet) {
        applyCustomFont(context, attributeSet);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);

            Typeface customFont = selectTypeface(context, textStyle);
            setTypeface(customFont);
        }
    }

    private Typeface selectTypeface(Context context, int textStyle) {
        /*
        * information about the TextView textStyle:
        * http://developer.android.com/reference/android/R.styleable.html#TextView_textStyle
        */
        switch (textStyle) {
            case Typeface.BOLD: // bold
                return FontCache.getTypeface("font/font-bold.ttf", context);
            case Typeface.ITALIC: // italic
                return FontCache.getTypeface("font/font-italic.ttf", context);
            case Typeface.BOLD_ITALIC: // bold italic
                return FontCache.getTypeface("font/ffont-boldItalic.ttf", context);
            case Typeface.NORMAL: // regular
                return FontCache.getTypeface("font/font-regular.ttf", context);
            default:
                return FontCache.getTypeface("font/font-regular.ttf", context);
        }
    }


}
