package jzel.theo.co.za.todo.models;

import java.util.Date;

public class TaskModel {

    public int id;
    public int userID;
    public String description;
    public boolean isComplete;
    public boolean isImportant;
    public Date createdDate;
    public Date reminderDate;

}
