package jzel.theo.co.za.todo.activities.splash;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import jzel.theo.co.za.todo.R;
import jzel.theo.co.za.todo.globals.StringGlobals;
import jzel.theo.co.za.todo.helpers.ConnectionsHelper;
import jzel.theo.co.za.todo.helpers.EmailValidationHelper;
import jzel.theo.co.za.todo.models.ApiResponse;
import jzel.theo.co.za.todo.models.UserModel;

public class SplashPresenter implements SplashContract.SplashPresenter {

    private SplashContract.SplashView view;
    private SplashActivity activity;
    private SharedPreferences sharedPreferences;
    private Gson gson;

    SplashPresenter(SplashContract.SplashView view,
                    SplashActivity activity,
                    SharedPreferences sharedPreferences)
    {
        this.view = view;
        this.activity = activity;
        this.sharedPreferences = sharedPreferences;
        gson = new Gson();
    }

    @Override
    public void addUser() {
        view.showLoading();
        if (EmailValidationHelper.isValidEmail(((EditText)activity.findViewById(R.id.cell_email_text)).getText().toString())) {
            // Continue to app, the following will add the user to the DB if they dont exist
            activity.findViewById(R.id.progress_view).setVisibility(View.VISIBLE);
            ConnectionsHelper.GET(activity, activity.getString(R.string.server)
                    + activity.getString(R.string.add_user_by_email)
                    + ((EditText) activity.findViewById(R.id.cell_email_text)).getText().toString(), new ConnectionsHelper.VolleyCallbackGET() {
                @Override
                public void onSuccess(String result, VolleyError error) {
                    view.hideLoading();
                    if (result != null) {
//                        ApiResponse response = gson.fromJson(result, ApiResponse.class);
//                        UserModel user = UserModel.class.cast(response.result.data);
                        try {
                            JSONObject res = new JSONObject(result);
                            sharedPreferences.edit().putString(StringGlobals.EMAIL, ((EditText)activity.findViewById(R.id.cell_email_text)).getText().toString()).apply();
                            sharedPreferences.edit().putInt(StringGlobals.USER_ID, (int)res.getJSONObject("result").getJSONObject("data").getInt("id")).apply();
                            view.openMainIntent();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        view.showMessage(activity.getString(R.string.generic_oops));
                    }
                }
            }, new HashMap<String, String>());
        } else {
            // Invalid email
            view.showMessage(activity.getString(R.string.invalid_email));
        }
    }
}
