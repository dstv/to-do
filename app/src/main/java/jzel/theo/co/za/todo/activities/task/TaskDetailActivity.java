package jzel.theo.co.za.todo.activities.task;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import jzel.theo.co.za.todo.R;

public class TaskDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);
    }
}
