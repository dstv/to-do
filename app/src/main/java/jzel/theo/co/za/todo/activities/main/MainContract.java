package jzel.theo.co.za.todo.activities.main;

import java.util.List;

import jzel.theo.co.za.todo.models.TaskModel;

public interface MainContract {

    interface MainPresenter {

        void getAllTasksForUser();

        void setDrawer();

        void setListItems(List<TaskModel> tasks);

        void openAddTaskView(TaskModel taskModel);

        void setClicks();

        void updateTask(TaskModel task);

        void deleteTask(int taskID);

        void signOut();
    }

    interface MainView {

        void showLoading();

        void hideLoading();

        void initUI();

        void showMessage(String message);

    }

}
