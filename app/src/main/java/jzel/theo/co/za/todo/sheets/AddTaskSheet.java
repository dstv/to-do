package jzel.theo.co.za.todo.sheets;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import jzel.theo.co.za.todo.R;
import jzel.theo.co.za.todo.activities.main.MainPresenter;
import jzel.theo.co.za.todo.dialogs.DeleteTaskDialog;
import jzel.theo.co.za.todo.globals.StringGlobals;
import jzel.theo.co.za.todo.helpers.ConnectionsHelper;
import jzel.theo.co.za.todo.helpers.DateHelper;
import jzel.theo.co.za.todo.models.TaskModel;
import jzel.theo.co.za.todo.serializers.DateDeserializer;

public class AddTaskSheet extends BottomSheetDialogFragment {

    private MainPresenter presenter;
    private Date reminderDate;
    private TaskModel task;
    private View contentView;

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        contentView = View.inflate(getContext(), R.layout.sheet_add_task, null);
        dialog.setContentView(contentView);

        final SharedPreferences sharedPreferences = getActivity().getSharedPreferences(StringGlobals.SHARED_PREFS, Context.MODE_PRIVATE);

        Bundle args = getArguments();
        try {
            GsonBuilder builder = new GsonBuilder();
//            builder.registerTypeAdapter(Date.class, new DateDeserializer());
            Gson gson = builder.create();
            task = gson.fromJson(args.getString("item"), TaskModel.class);
            if (task != null) {
                updateUI();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        ((Button) contentView.findViewById(R.id.due_date_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialDatePicker.Builder<Long> builder =
                        MaterialDatePicker.Builder.datePicker();
                final MaterialDatePicker<Long> picker = builder.build();
                picker.show(getFragmentManager(), picker.toString());
                picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
                    @Override
                    public void onPositiveButtonClick(Long selection) {
                        picker.dismiss();
                        ((Button) contentView.findViewById(R.id.due_date_button)).setText(DateHelper.getDDMMYYYYFromMilli(selection));
                        reminderDate = DateHelper.getDateFromMilli(selection);
                    }
                });
            }
        });

        ((Button) contentView.findViewById(R.id.add_task_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((EditText) contentView.findViewById(R.id.task_text)).getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), "Please enter your task description.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (reminderDate == null) {
                    Toast.makeText(getContext(), "Please choose a due date.", Toast.LENGTH_SHORT).show();
                    return;
                }

                contentView.findViewById(R.id.progress_view).setVisibility(View.VISIBLE);
                if (task == null) {
                    task = new TaskModel();
                    task.isComplete = false;
                }
                task.isImportant = ((CheckBox) contentView.findViewById(R.id.task_check_box)).isChecked();
                task.description = ((EditText) contentView.findViewById(R.id.task_text)).getText().toString();
                task.createdDate = new Date();
                task.reminderDate = reminderDate;
                task.userID = sharedPreferences.getInt(StringGlobals.USER_ID, -1);
                Gson gson = new Gson();
                String postDataString = gson.toJson(task);
                try {
                    JSONObject postData = new JSONObject(postDataString);
                    ConnectionsHelper.POST(getContext(), postData, new ConnectionsHelper.VolleyCallbackPOST() {
                        @Override
                        public void onSuccess(JSONObject result, VolleyError error) {
                            if (result != null) {
                                presenter.getAllTasksForUser();
                                dismiss();
                            } else {
                                Toast.makeText(getContext(), getString(R.string.generic_oops), Toast.LENGTH_LONG).show();
                            }
                        }
                    }, getString(R.string.server) + getString(R.string.add_task_for_user), new HashMap<String, String>());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        ((ImageView) contentView.findViewById(R.id.delete_task)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteTaskDialog.showAlert(getContext(), presenter, task);
                dismiss();
            }
        });
    }

    private void updateUI() {
        ((CheckBox) contentView.findViewById(R.id.task_check_box)).setChecked(task.isImportant);
        ((EditText) contentView.findViewById(R.id.task_text)).setText(task.description);
        reminderDate = task.reminderDate;
        ((Button) contentView.findViewById(R.id.due_date_button)).setText(DateHelper.getDDMMYYYYFromMilli(reminderDate.getTime()));
        ((Button) contentView.findViewById(R.id.add_task_button)).setText(getString(R.string.update));
        ((ImageView) contentView.findViewById(R.id.delete_task)).setVisibility(View.VISIBLE);
    }

    public void setPresenter(MainPresenter presenter) {
        this.presenter = presenter;
    }
}
