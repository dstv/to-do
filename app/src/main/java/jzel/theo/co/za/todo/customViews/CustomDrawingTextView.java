package jzel.theo.co.za.todo.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;


/**
 * Created by jzeltheophiluis on 2018/02/02.
 */

public class CustomDrawingTextView extends AppCompatTextView {

    private static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";
    private Context context;


    private CharSequence mText;
    private int mIndex;
    private long mDelay = 500; //Default 500ms delay


    public CustomDrawingTextView(Context context) {
        super(context);
        this.context = context;
        init(null);
    }

    public CustomDrawingTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    private Handler mHandler = new Handler();
    private Runnable characterAdder = new Runnable() {
        @Override
        public void run() {
            setText(mText.subSequence(0, mIndex++));
            if(mIndex <= mText.length()) {
                mHandler.postDelayed(characterAdder, mDelay);
            }
        }
    };

    public void animateText(CharSequence text) {
        mText = text;
        mIndex = 0;

        setText("");
        mHandler.removeCallbacks(characterAdder);
        mHandler.postDelayed(characterAdder, mDelay);
    }

    public void setCharacterDelay(long millis) {
        mDelay = millis;
    }


    public void init(AttributeSet attributeSet) {
        applyCustomFont(context, attributeSet);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);

            Typeface customFont = selectTypeface(context, textStyle);
            setTypeface(customFont);
        }
    }

    private Typeface selectTypeface(Context context, int textStyle) {
        /*
        * information about the TextView textStyle:
        * http://developer.android.com/reference/android/R.styleable.html#TextView_textStyle
        */
        switch (textStyle) {
            case Typeface.BOLD: // bold
                return FontCache.getTypeface("font/font_bold.ttf", context);
            case Typeface.ITALIC: // italic
                return FontCache.getTypeface("font/font_light.ttf", context);
            case Typeface.BOLD_ITALIC: // bold italic
                return FontCache.getTypeface("font/font_bold.ttf", context);
            case Typeface.NORMAL: // regular
                return FontCache.getTypeface("font/font_light.ttf", context);
            default:
                return FontCache.getTypeface("font/font_light.ttf", context);
        }
    }

}
