package jzel.theo.co.za.todo.globals;

public class StringGlobals {

    public static String SHARED_PREFS = "TODO_PREFS";
    public static String EMAIL = "EMAIL";
    public static String USER_ID = "USER_ID";
}
