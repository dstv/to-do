package jzel.theo.co.za.todo.helpers;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ConnectionsHelper {

    final private static int TIMEOUT_MS = 500000;

    public static String getErrorFromVolleyError(VolleyError error) {
        if (error.networkResponse != null && error.networkResponse.data != null) {
            String repsonseString = new String(error.networkResponse.data);
            try {
                JSONObject errorObject = new JSONObject(repsonseString);
                return errorObject.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "Oop! Something went wrong, please try again later";
    }


    public static interface VolleyCallbackGET {
        void onSuccess(String result, VolleyError error);
    }

    public static interface VolleyCallbackPOST {
        void onSuccess(JSONObject result, VolleyError error);
    }

    public static void POST (Context activity, JSONObject data,
                             final VolleyCallbackPOST callback,
                             String url,
                             final HashMap<String, String> headers) {
        RequestQueue queue = Volley.newRequestQueue(activity);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onSuccess(response, null);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(null, error);
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(jsonObjReq);
    }


    public static void GET (Context activity,
                            String url,
                            final VolleyCallbackGET callbackGET,
                            final HashMap<String, String> headers) {
        RequestQueue queue = Volley.newRequestQueue(activity);
        Log.wtf("GET_URL: ", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        callbackGET.onSuccess(response, null);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callbackGET.onSuccess(null, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }
}
