package jzel.theo.co.za.todo.customViews;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import jzel.theo.co.za.todo.R;


/**
 * Created by jzeltheophiluis on 2018/02/07.
 */

public class CustomProgressView extends AppCompatImageView {


    private ObjectAnimator anim;


    public CustomProgressView(Context context) {
        super(context);
        init(context);
    }

    public CustomProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);

    }

    public CustomProgressView(Context context, AttributeSet attrs, int theme) {
        super(context, attrs, theme);
        init(context);
    }

    private void init (Context context) {
        anim = (ObjectAnimator) AnimatorInflater.loadAnimator(context, R.animator.flip);
    }

    @Override
    public void setVisibility (int visibility) {
        super.setVisibility(visibility);
        if (visibility == VISIBLE) {
            anim.setTarget(this);
            anim.setDuration(1000);
            anim.setRepeatCount(ValueAnimator.INFINITE);
            anim.start();
        } else {
            anim.removeAllListeners();
            anim.cancel();
            anim.end();
        }
    }
}
