package jzel.theo.co.za.todo.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {

    public static String FormatDateYYYYMMDD (Date date) {
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
        return fmtOut.format(date);
    }

    public static String FormatDateDDMMMYYYY (Date date) {
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM yyyy");
        return fmtOut.format(date);
    }

    public static String getDDMMYYYYFromMilli (long milliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM YYYY");

        return simpleDateFormat.format(calendar.getTime());
    }

    public static Date getDateFromMilli (long milliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        return calendar.getTime();
    }
}
