package jzel.theo.co.za.todo.activities.splash;

public interface SplashContract {

    interface SplashPresenter {
        void addUser();
    }

    interface SplashView {

        void showLoading();

        void hideLoading();

        void showMessage(String message);

        void openMainIntent();

        void animateView();

    }
}
