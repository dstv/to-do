package jzel.theo.co.za.todo.helpers;

import android.text.TextUtils;
import android.util.Patterns;

public class EmailValidationHelper {

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
