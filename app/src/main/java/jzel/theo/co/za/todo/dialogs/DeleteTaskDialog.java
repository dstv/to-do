package jzel.theo.co.za.todo.dialogs;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import jzel.theo.co.za.todo.activities.main.MainPresenter;
import jzel.theo.co.za.todo.models.TaskModel;

public class DeleteTaskDialog {

    public static void showAlert (final Context context, final MainPresenter presenter, final TaskModel task) {
        new MaterialAlertDialogBuilder(context)
                .setTitle("Remove task")
                .setMessage("Are you sure you want to remove this task?")
                .setPositiveButton("REMOVE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.deleteTask(task.id);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }
}
