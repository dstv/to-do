package jzel.theo.co.za.todo.activities.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.LoginFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import jzel.theo.co.za.todo.R;
import jzel.theo.co.za.todo.activities.splash.SplashActivity;
import jzel.theo.co.za.todo.adapters.TasksListAdapter;
import jzel.theo.co.za.todo.globals.StringGlobals;
import jzel.theo.co.za.todo.helpers.ConnectionsHelper;
import jzel.theo.co.za.todo.models.ApiResponse;
import jzel.theo.co.za.todo.models.TaskModel;
import jzel.theo.co.za.todo.serializers.DateDeserializer;
import jzel.theo.co.za.todo.sheets.AddTaskSheet;

public class MainPresenter implements MainContract.MainPresenter {

    private MainActivity activity;
    private MainContract.MainView view;
    private SharedPreferences sharedPreferences;
    private Gson gson;
    private MainPresenter thisPresenter;
    private TaskModel task;
    private TasksListAdapter adapter;
    private List<TaskModel> filteredTasks;
    private List<TaskModel> tasks;

    public MainPresenter(MainActivity activity, MainContract.MainView view, SharedPreferences sharedPreferences) {
        this.activity = activity;
        this.view = view;
        this.sharedPreferences = sharedPreferences;
        this.thisPresenter = this;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        gson = gsonBuilder.create();
        filteredTasks = new ArrayList<>();
    }

    @Override
    public void getAllTasksForUser() {
        view.showLoading();
        ConnectionsHelper.GET(activity, activity.getString(R.string.server)
                + activity.getString(R.string.get_tasks_by_email)
                + sharedPreferences.getString(StringGlobals.EMAIL, ""), new ConnectionsHelper.VolleyCallbackGET() {
            @Override
            public void onSuccess(String result, VolleyError error) {
                view.hideLoading();
                if (view != null) {
                    ApiResponse response = gson.fromJson(result, ApiResponse.class);
                    Type typeMyType = new TypeToken<ArrayList<TaskModel>>() {
                    }.getType();
                    tasks = gson.fromJson(gson.toJson(response.result.data), typeMyType);
                    setListItems(tasks);
                } else {
                    view.showMessage(activity.getString(R.string.generic_oops));
                }
            }
        }, new HashMap<String, String>());
    }

    @Override
    public void setDrawer() {
        DrawerLayout drawer;

        drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(activity, drawer, (Toolbar) activity.findViewById(R.id.toolbar), R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void setListItems(final List<TaskModel> tasks) {
        setProgressBar(tasks);
        adapter = new TasksListAdapter(activity, thisPresenter);
        adapter.setItems(tasks);
        ((ListView) activity.findViewById(R.id.list_view)).setAdapter(adapter);
        ((ListView) activity.findViewById(R.id.list_view)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                task = tasks.get(i);
//                openAddTaskView(task);
            }
        });
    }

    private void setProgressBar(List<TaskModel> tasks) {
        int completeCount = 0;
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).isComplete) {
                completeCount++;
            }
        }
        float completePercentage = ((float) completeCount / (float) tasks.size()) * 100;
        completePercentage = (int) completePercentage;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (completePercentage >= 0 && completePercentage <= 33) {
                ((ProgressBar) activity.findViewById(R.id.task_progress_bar)).setProgressTintList(ColorStateList.valueOf(activity.getResources().getColor(R.color.colorGoogleRed)));
            } else if (completePercentage > 34 && completePercentage <= 69) {
                ((ProgressBar) activity.findViewById(R.id.task_progress_bar)).setProgressTintList(ColorStateList.valueOf(activity.getResources().getColor(R.color.colorGoogleYellow)));
            } else {
                ((ProgressBar) activity.findViewById(R.id.task_progress_bar)).setProgressTintList(ColorStateList.valueOf(activity.getResources().getColor(R.color.colorGoogleGreen)));
            }
        }
        ((ProgressBar) activity.findViewById(R.id.task_progress_bar)).setProgress((int) completePercentage);
        ((TextView) activity.findViewById(R.id.percent_text)).setText((int) completePercentage + "%");
    }

    @Override
    public void openAddTaskView(TaskModel task) {
        final AddTaskSheet bottomSheetDialogFragment = new AddTaskSheet();
        if (task != null) {
            Bundle args = new Bundle();
            args.putString("item", gson.toJson(task));
            bottomSheetDialogFragment.setArguments(args);
        }
        bottomSheetDialogFragment.setPresenter(thisPresenter);
        bottomSheetDialogFragment.show(activity.getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    @Override
    public void setClicks() {
        activity.findViewById(R.id.add_task_fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                task = null;
                openAddTaskView(null);
            }
        });

        activity.findViewById(R.id.sign_out).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signOut();
            }
        });

        activity.findViewById(R.id.sort_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signOut();
            }
        });

        ((EditText)activity.findViewById(R.id.filter_list)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filterTaskList(editable.toString());
            }
        });
    }

    private void filterTaskList (String searchText) {
        filteredTasks.clear();
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).description.toLowerCase().contains(searchText)) {
                filteredTasks.add(tasks.get(i));
            }
        }
        adapter.setItems(filteredTasks);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void updateTask(TaskModel task) {
        try {
            Gson gson = new Gson();
            String postDataString = gson.toJson(task);
            JSONObject postData = new JSONObject(postDataString);
            ConnectionsHelper.POST(activity, postData, new ConnectionsHelper.VolleyCallbackPOST() {
                @Override
                public void onSuccess(JSONObject result, VolleyError error) {
                    if (result != null) {
                        getAllTasksForUser();
                    } else {
                        Toast.makeText(activity, activity.getString(R.string.generic_oops), Toast.LENGTH_LONG).show();
                    }
                }
            }, activity.getString(R.string.server) + activity.getString(R.string.add_task_for_user), new HashMap<String, String>());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void deleteTask(int taskID) {
        ConnectionsHelper.GET(activity, activity.getString(R.string.server)
                + activity.getString(R.string.delete_task_by_id)
                + taskID, new ConnectionsHelper.VolleyCallbackGET() {
            @Override
            public void onSuccess(String result, VolleyError error) {
                if (result != null) {
                    Toast.makeText(activity, activity.getString(R.string.deleted_task), Toast.LENGTH_LONG).show();
                    getAllTasksForUser();
                } else {
                    Toast.makeText(activity, activity.getString(R.string.generic_oops), Toast.LENGTH_LONG).show();
                }
            }
        }, new HashMap<String, String>());
    }

    @Override
    public void signOut() {
        sharedPreferences.edit().clear().apply();
        Intent intent = new Intent(activity, SplashActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }


}
