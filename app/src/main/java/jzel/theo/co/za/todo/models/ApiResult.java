package jzel.theo.co.za.todo.models;

public class ApiResult {

    public int status;
    public String message;
    public Object data;
}
