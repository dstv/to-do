package jzel.theo.co.za.todo.activities.task;

public interface TaskDetailContract {
    interface TaskDetailPresenter {
        void getTaskDetail();

    }

    interface TaskDetailView {

        void showLoading();

        void hideLoading();

        void showMessage(String message);


    }
}
