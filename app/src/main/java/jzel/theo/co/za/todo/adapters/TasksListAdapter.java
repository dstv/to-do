package jzel.theo.co.za.todo.adapters;

import android.app.Activity;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import jzel.theo.co.za.todo.R;
import jzel.theo.co.za.todo.activities.main.MainPresenter;
import jzel.theo.co.za.todo.helpers.DateHelper;
import jzel.theo.co.za.todo.models.TaskModel;

public class TasksListAdapter extends BaseAdapter {

    private Activity activity;
    private List<TaskModel> items;
    private MainPresenter presenter;

    public TasksListAdapter (Activity activity, MainPresenter presenter) {
        this.activity = activity;
        this.presenter = presenter;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = activity.getLayoutInflater().inflate(R.layout.list_task_item, null);
        }

        final TaskModel task = (TaskModel) items.get(i);

        ((CheckBox)view.findViewById(R.id.task_check_box)).setChecked(task.isComplete);
        final View finalView = view;
        ((CheckBox)view.findViewById(R.id.task_check_box)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                task.isComplete = b;
                if (b) {
                    ((TextView) finalView.findViewById(R.id.task_title)).setPaintFlags(((TextView) finalView.findViewById(R.id.task_title)).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                } else {
                    ((TextView) finalView.findViewById(R.id.task_title)).setPaintFlags(0);
                }
                presenter.updateTask(task);
            }
        });

        ((ImageView)view.findViewById(R.id.task_important)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (task.isImportant) {
                    ((ImageView)view.findViewById(R.id.task_important)).setColorFilter(activity.getResources().getColor(R.color.colorGreyLight));
                    task.isImportant = false;
                } else {
                    ((ImageView)view.findViewById(R.id.task_important)).setColorFilter(activity.getResources().getColor(R.color.colorPrimary));
                    task.isImportant = true;
                }
                presenter.updateTask(task);
            }
        });


        if (task.isImportant) {
            ((ImageView)view.findViewById(R.id.task_important)).setColorFilter(activity.getResources().getColor(R.color.colorPrimary));
        } else {
            ((ImageView)view.findViewById(R.id.task_important)).setColorFilter(activity.getResources().getColor(R.color.colorGreyLight));
        }

        if (task.isComplete) {
            ((TextView)view.findViewById(R.id.task_title)).setPaintFlags(((TextView)view.findViewById(R.id.task_title)).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            ((TextView)view.findViewById(R.id.task_title)).setPaintFlags(0);
        }

        ((TextView)view.findViewById(R.id.task_title)).setText(task.description);

        // Implemented this due to onitemclick not working
        ((TextView)view.findViewById(R.id.date_text)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.openAddTaskView(task);
            }
        });

        ((TextView)view.findViewById(R.id.task_title)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.openAddTaskView(task);
            }
        });

        ((TextView)view.findViewById(R.id.date_text)).setText(DateHelper.getDDMMYYYYFromMilli(task.reminderDate.getTime()));

        return view;
    }

    public void setItems(List<TaskModel> items) {
        this.items = items;
    }
}
