package jzel.theo.co.za.todo.activities.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import jzel.theo.co.za.todo.R;
import jzel.theo.co.za.todo.globals.StringGlobals;

public class MainActivity extends AppCompatActivity implements MainContract.MainView {

    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter(this, this, getSharedPreferences(StringGlobals.SHARED_PREFS, Context.MODE_PRIVATE));
        presenter.setDrawer();
        presenter.setClicks();
        presenter.getAllTasksForUser();

        // Set menu email text
        ((TextView)findViewById(R.id.cell_email_text)).setText(getSharedPreferences(StringGlobals.SHARED_PREFS, Context.MODE_PRIVATE).getString(StringGlobals.EMAIL, ""));

    }

    @Override
    public void showLoading() {
        findViewById(R.id.progress_view).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        findViewById(R.id.progress_view).setVisibility(View.GONE);
    }

    @Override
    public void initUI() {

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
}
